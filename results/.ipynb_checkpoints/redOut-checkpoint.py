import sys
import re


txt = sys.stdin.read()
first_line = re.match(".*\n", txt).group()[:-1]

results = re.findall("\d+.\d+", txt)
with open("./results-{0}.csv".format(first_line), 'a') as f:
    f.write(results[0])
    for i in range(1, len(results)):
        f.write(',')
        f.write(results[i])
    f.write('\n')
