import os
import re
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument(
    "--executable",
    "-e",
    type=str,
    default="../cmake-build-debug/CUDA_kernel_image_processing",
)
parser.add_argument("--mask_width", "-m", type=int, default=5)
parser.add_argument("--tile_width", "-t", type=int, default=32)
flags = parser.parse_args()

n_experiments = 30
for i in range(n_experiments):
    txt = os.popen(f"./{flags.executable}").read()
    first_line = re.match(".*\n", txt).group()[:-1]

    results = re.findall("\d+.\d+", txt)
    filename = "./res-{0}-{1}x{1}-{2}x{2}.csv".format(first_line, flags.mask_width, flags.tile_width)
    with open(filename, "a") as f:
        f.write(results[0])
        for i in range(1, len(results)):
            f.write(",")
            f.write(results[i])
        f.write("\n")
        
print(os.popen(f"wc -l {filename}").read())
