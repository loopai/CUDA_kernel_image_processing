#!/bin/bash

if [[ $(basename $(pwd)) != "data" ]]
then
    echo "This script must be run from ./data folder."
    exit 1
fi

# Downloading 100x100 image
if [[ ! -e "./1-img.ppm" ]]
then
    echo ">>> Downloading a 100x100 image as 1-img.jpg"
    wget -q https://upload.wikimedia.org/wikipedia/commons/5/50/Vd-Orig.png
    mv Vd-Orig.png 1-img.jpg
    echo ">>> Converting 1-img.jpg from .jpg to .ppm"
    convert 1-img.jpg 1-img.ppm
    rm 1-img.jpg
fi

# Downloading FHD image
if [[ ! -e "./2-img.ppm" ]]
then
    echo ">>> Downloading a 1920x1080 image as 2-img.jpg"
    wget -q https://www.chainimage.com/images/colorful-robots-images-free-wallpaper-1920x1080-full-hd-wallpapers.jpg  
    mv colorful-robots-images-free-wallpaper-1920x1080-full-hd-wallpapers.jpg 2-img.jpg
    echo ">>> Converting 2-img.jpg from .jpg to .ppm"
    convert 2-img.jpg 2-img.ppm
    rm 2-img.jpg
fi

# Downloading 4K image
if [[ ! -e "./3-img.ppm" ]]
then
    echo ">>> Downloading a 4096x2160 image as 3-img.jpg"
    wget -q https://images5.alphacoders.com/329/329544.jpg
    mv 329544.jpg 3-img.jpg
    echo ">>> Converting 3-img.jpg from .jpg to .ppm"
    convert 3-img.jpg 3-img.ppm
    rm 3-img.jpg
fi

# Downloading 8K image
if [[ ! -e "./4-img.ppm" ]]
then
    echo ">>> Downloading a 7680x4320 image as 4-img.jpg"
    wget -q https://images.alphacoders.com/968/968906.jpg
    mv 968906.jpg 4-img.jpg
    echo ">>> Converting 4-img.jpg from .jpg to .ppm"
    convert 4-img.jpg 4-img.ppm
    rm 4-img.jpg
fi

exit 0
